function [samples,group,svmStruct,results] = detector()
samples = [];
group = [];
for i = 1:50;
    path = strcat('/home/edvard/edvard/skola/bis/fld_roc/cover/',int2str(i),'.pgm');
    if exist(path);
        samples = [samples ; spam(path)'];
        group = [group; 0];
    end;
end;

samples = [samples ; spam('Lena.png')'];
group = [group; 1];

svmStruct = svmtrain(samples',group')


classification = [];
for i = 1:2000;
    path = strcat('/home/edvard/edvard/skola/bis/fld_roc/rank/',int2str(i),'.pgm');
    if exist(path);
        classification = [classification ; spam(path)'];                
    end;
end;
results = svmclassify(svmStruct, classification, 'Showplot',true)

%dlmwrite('output.txt',results)

end