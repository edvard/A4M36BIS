function [f] = spam(image_name)
image = int16(imread(image_name));
[w h] = size(image);
imageDiff = zeros(w-1,h-1); % odectu sousedy

bound = 7;

histog = zeros(bound*2+1,1);

% prvni krok - odecist sousedy vpravo a dole
for x = 1:w-1;
  for y = 1:h-1;      
      newVal = int16((image(x+1,y)-image(x,y) + image(x,y+1)-image(x,y) ) / 2);
      imageDiff(x,y) = newVal;
      key = newVal;
      if key < -bound;                              
          key = -bound;
      end;
      if key > bound;
          key = bound;
      end;
      if key == -bound ;          
      end;
      key = key + bound +1;
      % positiv integer, nebot debilni matlab neumi zaporne indexy
      histog(key) = histog(key) + 1;
  end;  
  break;
end;
plot(histog)
axis([0,21,0,200]) % normalni lidske meritko
f = histog;
end