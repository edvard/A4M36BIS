function [message] = extract(inputfile,key)

rng(key);
bitPos = randperm(8);
%bitPos = [1 1 1 1 1 1 1 1]
%msgLength = 262144+ 32;

input=imread(inputfile);
[w h] = size(input);
message = [];
msgPos = 0;
msgLength = 0;
msgLengthPos = 0;
msgLengthBits = [];

i = 0;
for x = 1:w;
  for y = 1:h;
    i=i+1;    
    % dekodujeme msgLength    
    if(msgLengthPos < 32); % kodujeme msgLength
      msgLengthPos = msgLengthPos+1;            
      msgLengthBits = [msgLengthBits bitget(input(x,y), bitPos(mod(i,8)+1))];
      if(msgLengthPos == 32);      	
	for i2=0:31;
	  msgLength = msgLength+double(msgLengthBits(i2+1))*2^i2;
	end;		

      end;      
      continue;    
    end;
    
    if(msgPos < msgLength); % dekodujeme zpravu    
      msgPos=msgPos+1;          
      message = [message; bitget(input(x,y), bitPos(mod(msgPos,8)+1))];            
    end;
    
    if(msgPos >= msgLength);
	break; % v matlabu nejde break multiple loops
    end;    
  end;  
end;

if(msgPos < msgLength); 
  for(i=msgPos+1:msgLength);
    message = [message; 0];            
  end;
  fprintf('!!Image was too small to accept stego!!\n');
end;
  


  
end