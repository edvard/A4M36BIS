function [] = embed(image,message,key,outputfile)

rng(key);
bitPos = randperm(8);
%bitPos = [1 1 1 1 1 1 1 1]

original=imread(image);
[w h] = size(original);
output = zeros([w,h], 'uint8');

msgPos = 0;
msgLength = size(message,1);
msgLengthPos = 0;
msgLengthBits = de2bi(msgLength,32);

i = 0;
for x = 1:w;
  for y = 1:h;    
    i = i+1;
  
    if(msgLengthPos < 32); % kodujeme msgLength
      msgLengthPos = msgLengthPos+1;
      output(x,y) = bitset(original(x,y), bitPos(mod(i,8)+1), msgLengthBits(msgLengthPos));
      continue;
    end;
  
    if(msgPos < msgLength ); % kodujeme zpravu	
	msgPos=msgPos+1;
	output(x,y) = bitset(original(x,y), bitPos(mod(i,8)+1), message(msgPos));      	
	continue;           
    end;
    % ve zbylem miste kopirujeme puvodni obrazek
    output(x,y) = original(x,y);            
  end;
end;
imwrite(output,outputfile);

end